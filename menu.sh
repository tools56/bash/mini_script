#!/usr/bin/env bash
echo "Liste des containers present"

Liste=$(docker ps -a --format '{{.Names}}' | grep -i "jenkins")

declare -a TabVar
typeset -i Ind=0

while IFS= read -r line
do
   ((Ind=$Ind+1))
   lstNames[$Ind]=$line
   echo "${Ind} : $line"
done <<< "${Liste}";

typeset -i IndMax=$Ind
Ind=0
echo -n "Votre choix (Abandon : <Ctrl-c>) : "
read choix
container=${lstNames[$choix]}
if [[ ! -z ${container// /} ]]; then
   docker start "${container}"
   docker exec -it "${container}" zsh
fi
exit 0